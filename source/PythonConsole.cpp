// include sdk headers to communicate with UE3
// WARNING: this header file can currently only be included once!
//   the SDK currently throws alot of warnings which can be ignored
#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <vector>
#include <SdkHeaders.h>


// for access to event manager
#include <Proxy/Events.h>
// for access to client/server
#include <Proxy/Network.h>
// for module api 
#include <Proxy/Modules.h>
// for logging in main file
#include <Proxy/Logger.h>
#include <Proxy/Console.h>

using namespace BLRevive;

typedef void(*TPyInit)();
typedef int(*TPySS)(const char*);
typedef int(*TPyIL)(FILE*, const char*);
typedef int(*TPySF)(FILE*, const char*);

struct PythonModuleConfig : ConfigProvider
{
    std::string PythonPath = "./Modules/python/embed";
    std::string ScriptsPath = "./Modules/python/scripts";
    std::string InitScript = "./Modules/python/init.py";
};

/// <summary>
/// Thread thats specific to the module (function must exist and export demangled!)
/// </summary>
extern "C" __declspec(dllexport) void ModuleThread()
{
    auto config = PythonModuleConfig();

    // load local python3.dll at runtime to allow using embed python
    //  this prevents the user to be forced to install python
    HINSTANCE hPythonDll = LoadLibraryA((config.PythonPath + "/python311.dll").c_str());

    if (!hPythonDll) 
    {
        LError("Python was not found in PythonPath: {}!", config.PythonPath);
        return;
    }

    // get function addresses for usage
    auto Py_Initialize = (TPyInit)GetProcAddress(hPythonDll, "Py_Initialize");
    auto PyRun_SimpleString = (TPySS)GetProcAddress(hPythonDll, "PyRun_SimpleString");
    auto PyRun_InteractiveLoop = (TPyIL)GetProcAddress(hPythonDll, "PyRun_InteractiveLoop");
    auto PyRun_SimpleFile = (TPySF)GetProcAddress(hPythonDll, "PyRun_SimpleFile");

    // check that all functions have been found
    if (!Py_Initialize || !PyRun_SimpleString || !PyRun_InteractiveLoop) {
        LError("Python is missing necessary function exports!\n\tInit: {}\n\tSimpleString: {}\n\tInteractiveLoop: {}\n\tSimpleFile: {}",
            (DWORD)Py_Initialize, (DWORD)PyRun_SimpleString, (DWORD)PyRun_InteractiveLoop, (DWORD)PyRun_SimpleFile);
        return;
    }

    LDebug("Initializing embed python...");
    Py_Initialize();

    LDebug("Python: Running initializiation script {}...", config.InitScript);
    FILE* hFile = fopen(config.InitScript.c_str(), "r");
    PyRun_SimpleFile(hFile, (config.InitScript).c_str());

    LDebug("Python: Initialization finished, starting interactive shell");
    PyRun_InteractiveLoop(stdin, "<stdin>");
}

/// <summary>
/// Module initializer (function must exist and export demangled!)
/// </summary>
/// <param name="data"></param>
extern "C" __declspec(dllexport) void InitializeModule(Module::InitData *data)
{
    // check param validity
    if (!data || !data->EventManager || !data->Logger) {
        LError("module initializer param was null!"); LFlush;
        return;
    }

    // initialize logger (to enable logging to the same file)
    Logger::Link(data->Logger);

    // initialize event manager
    // an instance of the manager can be retrieved with Events::Manager::Instance() afterwards
    Event::Manager::Link(data->EventManager);
    BLRE::CLI::Session::Link(data->CLI);
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

